﻿# Test Data for TIAD 2017 Shared Task

This repository contains the test data used for submissions to the [TIAD 2017 Shared Task](https://tiad2017.wordpress.com/).

## Contents

This directory contains four subdirectories:

* DE_EN_BR_DE
* DE_JA_ES_BR_DE
* DE_DK_FR_ES_BR_DE
* DE_NL_ES_DK_FR_BR_DE

The subdirectories contain data for the following paths, respectively:

* German>English | English>Portuguese
* German>Japanese | Japanese>Spanish | Spanish>Portuguese
* German>Danish | Danish>French | French>Spanish | Spanish>Portuguese
* German>Dutch | Dutch>Spanish | Spanish>Danish | Danish>French | French>Portuguese

Also included are four Portuguese>German datasets, for closing the loop in each path.

There are 14 columns in each file (except for Japanese, see below):

1. **SEid** - sense_id - each sense has its own id, and it allows for tracking id numbers if one wishes to start with one sense in German and end with the same sense after closing the loop. 
1. **HWord** - the headword of the language from which we translate, always corresponds to the first language in the end of file names
 * de_dk_fr_es_br_de-brde.tsv - in this case Brazilian Portuguese
1. **POS**	- part-of-speech of HWord
1. **HwGG** - grammatical gender of Hword (if exists)	
1. **HwGN** - grammatical number of Hword
1. **TransSrc** - translation to the target language
 * de_dk_fr_es_br_de-brde.tsv => German	
1. **TransPOS** - part-of-speech of TransSrc
1. **TransGG**	- grammatical gender of TransSrc 
1. **TransGN**	grammatical number of TransSrc 
1. **Synonym**	- synonym of HWord
1. **SubjectField** - domain of HWord
1. **Subcategorization** - subcategorization frame / valency of HWord
1. **Example**	- example sentence of HWord
1. **ExampleTrans** - translation of the example sentence of HWord

Note that for Japanese we tried to cover different scripts, given they exist in our data. We have noted some problems with the Japanese pairs, but the nature of these problems reflect problems which may arise in every dictionary, and therefore we define it as part of the challenge. 

Also note that not in all cases a loop is closed from German to German - again, this is the case for any dictionary set, and therefore, relying solely on closing the loop as a methodology is not enough. 

## Contact Us

For further questions, and if you encounter any problem with the data, please contact noam@kdictionaries.com